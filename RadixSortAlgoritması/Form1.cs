﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadixSortAlgoritması
{
    public partial class Form1 : Form
    {
        int elemansayisi =20;
        public Form1()
        {
            InitializeComponent();
        }

        //Sayfa açıldığında olacakları yazıyoruz
        private void Form1_Load(object sender, EventArgs e)
        {
            Random rastgele = new Random();
            int[] dizi = new int[elemansayisi];
            for (int i = 0; i < elemansayisi; i++)
            {
                dizi[i] = rastgele.Next(999);

            }
            for(int i=0;i<elemansayisi;i++)
            { 
                lstRastgele.Items.Add(dizi[i].ToString());

            }
            
            label2.Text = "En Büyük Eleman :  "+EnBuyukElemanBul(dizi,elemansayisi).ToString();
            int[] sonuc=Radix_Sort(dizi);
            for(int i=0;i<elemansayisi;i++)
            {
                lstSonuclar.Items.Add(sonuc[i].ToString());

            }
            
        }

        // En büyük elemanı bulan fonksiyonu yazıyoruz
        private int EnBuyukElemanBul(int[] dizi,int elemansayisi)
        {
            int maxeleman=0;
           for(int i=0;i<elemansayisi;i++)
            { 
                if(dizi[i]>maxeleman)
                {
                    maxeleman = dizi[i];
                }
                
            }
            return maxeleman;
        }

        // Radix Sort Algoritmasını yazıyoruz
        static int[] Radix_Sort(int[] dizi, int b = 1)
        {
            int[] a = new int[10];
            int[] c = new int[dizi.Length];

            int i = 0, ra = 0, p = b * 10, k = 0;

            //Rakamlara göre say
            for (i = 0; i < dizi.Length; i++)
            {
                ra = (dizi[i] % p) / b;

                //Kaçıncı basamağındaysa en büyük sayıyı al
                if (dizi[i] / p > k)
                    k = dizi[i] / p;
                a[ra]++;
            }

            //n = n + (n - 1)
            for (i = 1; i < a.Length; i++)
            {
                a[i] += a[i - 1];
            }

            //a daki indexlere göre sayıları sırala
            for (i = dizi.Length - 1; i >= 0; i--)
            {
                ra = (dizi[i] % p) / b;
                c[--a[ra]] = dizi[i];
            }

            //Sayı sıfırdan büyükse 
            if (k > 0)
                return Radix_Sort(c, p);
            return c;
        }

        //Çıkış butonuna basıldığında ki olayları yazıyorum
        private void btnCikis_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Çıkmak istediğinizden emin misiniz ?","UYARI!!!",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)==DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
